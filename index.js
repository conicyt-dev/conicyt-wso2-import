//process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var _ = require('lodash');
var request = require("request");
var fs = require("fs");
var q = require('q');

module.exports = function (apiDir, amSocket, amUsername, amPassword, endpoint) {
    var defer = q.defer();

    var dir = apiDir;

    if (isDirectory(dir)) {
        fs.readdir(dir, function (err, files) {
            if (err) {
                console.error("No se pudo listar el directorio.", err);
                defer.reject();
            }
            else {

                var metaDir = dir + "/Meta-information/";
                var swagger = JSON.parse(fs.readFileSync(metaDir + "swagger.json", 'utf-8'));
                var api = JSON.parse(fs.readFileSync(metaDir + "api.json", 'utf-8'));


                var amManager = new AMManager(api, swagger, amSocket, amUsername, amPassword, endpoint);

                var cookie = '';

                amManager.amLogin(function (error, response, body) {
                    try {
                        var objBody = JSON.parse(body);
                    }
                    catch (e) {
                        console.log('error', body);
                        defer.reject();
                        return;
                    }

                    if (objBody.error) {
                        console.log('error LOGIN body: ', objBody.message);
                        defer.reject();
                    }
                    else {

                        cookie = response.headers['set-cookie'];

                        amManager.amGetAPI(cookie, function (error, response, body) {

                            var objBody = JSON.parse(body);

                            if (objBody.error) {
                                console.log('API does not exist\n');
                                amManager.amAddAPI(cookie, function (error, response, body) {
                                    body = JSON.parse(body);

                                    if (body.error) {
                                        console.log('Error adding API::->>', body);
                                        defer.reject();
                                    }
                                    else {
                                        console.log('API Added ');
                                        amManager.amUpdateStatus(cookie,
                                            function (error, response, body) {
                                                var objBody = JSON.parse(body);
                                                if (objBody.error) {
                                                    console.log('error PUBLISH body: ', objBody.message);
                                                    defer.reject();
                                                }
                                                else {
                                                    console.log('API Published');
                                                    // logout
                                                    amManager.amLogout(
                                                        function (error, response, body) {
                                                            var objBody = JSON.parse(body);
                                                            if (objBody.error) {
                                                                console.log('error LOGOUT body: ', objBody.message);
                                                                defer.reject();
                                                            }
                                                            else {
                                                                defer.resolve();
                                                            }
                                                        });
                                                }
                                            });
                                    }
                                });
                            }
                            else {
                                console.log('API exists');
                                amManager.amUpdateAPI(cookie, function (error, response, body) {
                                    var objBody = JSON.parse(body);

                                    if (objBody.error) {
                                        console.log('Error updating API', objBody.message);
                                        defer.reject();
                                    }
                                    else {
                                        console.log('Updated');
                                        amManager.amUpdateStatus(cookie,
                                            function (error, response, body) {
                                                var objBody = JSON.parse(body);
                                                if (objBody.error) {
                                                    console.log('error PUBLISH body: ', objBody.message);
                                                    defer.reject();
                                                } else {
                                                    console.log('Published');
                                                    // logout
                                                    amManager.amLogout(
                                                        function (error, response, body) {
                                                            var objBody = JSON.parse(body);
                                                            if (objBody.error) {
                                                                console.log('error LOGOUT body: ', objBody.message);
                                                                defer.reject();
                                                            }
                                                            else {
                                                                defer.resolve();
                                                            }
                                                        });
                                                }
                                            });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    else {
        console.log("Usage: node index.js <path query>");
        defer.reject();
    }

    return defer.promise;
};

// utils
function isDirectory(filePath) {
    try {
        return fs.statSync(filePath).isDirectory();
    }
    catch (err) {
        return false;
    }
}

//----


var AMManager = function (api, swagger, amSocket, amUsername, amPassword, endpoint) {
    this.api = api;
    this.swagger = swagger;
    this.amSocket = amSocket;
    this.amUsername = amUsername;
    this.amPassword = amPassword;
    this.apiName = api.id.apiName;
    this.apiVersion = api.id.version;
    this.apiProvider = api.id.providerName;
    this.endpoint = endpoint;
};


AMManager.prototype.amLogout = function (callback) {
    request({
        uri: this.amSocket + "/publisher/site/blocks/user/login/ajax/login.jag?action=logout",
        method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
    }, callback);
};

AMManager.prototype.amUpdateStatus = function (cookie, callback) {
    request({
        uri: this.amSocket + "/publisher/site/blocks/life-cycles/ajax/life-cycles.jag",
        method: "POST",
        headers: {
            Cookie: cookie
        },
        form: {
            action: 'updateStatus',
            name: this.apiName,
            version: this.apiVersion,
            provider: this.apiProvider,
            status: 'PUBLISHED',
            publishToGateway: 'true',
            requireResubscription: 'true'
        }
    }, callback);
};

AMManager.prototype.amLogin = function (callback) {
    request({
        uri: this.amSocket + "/publisher/site/blocks/user/login/ajax/login.jag",
        method: "POST",
        form: {
            action: "login",
            username: this.amUsername,
            password: this.amPassword
        }
    }, callback);
};

AMManager.prototype.amGetAPI = function (cookie, callback) {
    request({
        uri: this.amSocket + "/publisher/site/blocks/listing/ajax/item-list.jag",
        method: "POST",
        headers: {
            Cookie: cookie
        },
        form: {
            action: 'getAPI',
            name: this.apiName,
            version: this.apiVersion,
            provider: this.apiProvider
        }
    }, callback);
};

AMManager.prototype.amUpdateAPI = function (cookie, callback) {
    var endpointConfig = {
        production_endpoints: {
            url: this.endpoint,
            config: null
        },
        implementation_status: "managed",
        endpoint_type: "http"
    };


    var form = {
        action: 'updateAPI',
        name: this.apiName,
        version: this.apiVersion,
        provider: this.apiProvider,
        context: _.replace(this.api.context, '/' + this.apiVersion, ''),
        visibility: this.api.visibility,
        tags: this.api.tags,
        thumbUrl: this.api.thumbUrl,
        description: this.api.description,
        availableTiers: this.api.availableTiers,
        endpointType: this.api.endpointType,
        endpoint_config: JSON.stringify(endpointConfig),
        http_checked: this.api.http_checked,
        https_checked: this.api.https_checked,
        //tiersCollection: 'Gold',//this.api.tiersCollection,
        swagger: JSON.stringify(this.swagger)
    };

    if (form.visibility == 'restricted') {
        form.roles = this.api.visibleRoles
    }

    request({
        uri: this.amSocket + "/publisher/site/blocks/item-add/ajax/add.jag",
        method: "POST",
        headers: {
            Cookie: cookie
        },
        form: form
    }, callback);
};

AMManager.prototype.amAddAPI = function (cookie, callback) {
    var endpointConfig = {
        production_endpoints: {
            url: this.endpoint,
            config: null
        },
        implementation_status: "managed",
        endpoint_type: "http"
    };


    var form = {
        action: 'addAPI',
        name: this.apiName,
        version: this.apiVersion,
        //provider: this.apiProvider,
        context: _.replace(this.api.context, '/' + this.apiVersion, ''),
        visibility: this.api.visibility,
        tags: this.api.tags,
        thumbUrl: this.api.thumbUrl,
        description: this.api.description,
        //availableTiers: this.api.availableTiers,
        endpointType: this.api.endpointType,
        endpoint_config: JSON.stringify(endpointConfig),
        http_checked: this.api.http_checked,
        https_checked: this.api.https_checked,
        tiersCollection: 'Gold,Unlimited',
        swagger: JSON.stringify(this.swagger)
    };

    if (form.visibility == 'restricted') {
        form.roles = this.api.visibleRoles
    }

    request({
        uri: this.amSocket + "/publisher/site/blocks/item-add/ajax/add.jag",
        method: "POST",
        headers: {
            Cookie: cookie
        },
        form: form
    }, callback);
};